﻿using GestionBancaireFrontEnd.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace GestionBancaireFrontEnd.Controllers
{
    public class CardTypeController : Controller
    {
        HttpClient httpClient;

        public CardTypeController()
        {
            httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri("http://localhost:8080/");
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        // GET: CardType
        public ActionResult Index()
        {
            var response = httpClient.GetAsync("bee2be/servlet/api/cardType").Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsAsync<List<CardType>>().Result;
                return View(content);
            }
            else
            {
                return View(new List<CardType>());
            }
        }

        // GET: CardType/Details/5
        public ActionResult Details(int id)
        {
            var response = httpClient.GetAsync("bee2be/servlet/api/cardType/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsAsync<CardType>().Result;
                return View(content);
            }
            else
            {
                return View(new CardType());
            }
        }

        // GET: CardType/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CardType/Create
        [HttpPost]
        public ActionResult Create(CardType cardType)
        {
            try
            {
                var response = httpClient.PostAsJsonAsync("bee2be/servlet/api/cardType", cardType).
                    ContinueWith(task => task.Result.EnsureSuccessStatusCode());

                return RedirectToAction("Index");

            }
            catch
            {
                return View();
            }
        }

        // GET: CardType/Edit/5
        public ActionResult Edit(int id)
        {
            var response = httpClient.GetAsync("bee2be/servlet/api/cardType/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsAsync<CardType>().Result;
                return View(content);
            }
            else
            {
                return View(new CardType());
            }
        }

        // POST: CardType/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, CardType cardType)
        {
            try
            {
                var response = httpClient.PutAsJsonAsync("bee2be/servlet/api/cardType/" + id, cardType).
                    ContinueWith(task => task.Result.EnsureSuccessStatusCode());

                return RedirectToAction("Index");

            }
            catch
            {
                return View();
            }
        }

        // GET: CardType/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CardType/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
