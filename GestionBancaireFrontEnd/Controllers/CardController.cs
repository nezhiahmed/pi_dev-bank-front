﻿using GestionBancaireFrontEnd.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace GestionBancaireFrontEnd.Controllers
{
    public class CardController : Controller
    {
        HttpClient httpClient;
        public CardController()
        {
            httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri("http://localhost:8080/");
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        
        // GET: Card
        public ActionResult Index()
        {
            var response = httpClient.GetAsync("bee2be/servlet/api/card").Result;
            if (response.IsSuccessStatusCode) { 
                var content = response.Content.ReadAsAsync<List<Card>>().Result;
                return View(content);
            }
            else {
                return View(new List<Card>());
            }
            
        }

        // GET: Card/Details/5
        public ActionResult Details(int id)
        {
            var response = httpClient.GetAsync("bee2be/servlet/api/card/"+id).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsAsync<Card>().Result;
                return View(content);
            }
            else
            {
                return View(new Card());
            }
        }

        // GET: Card/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Card/Create
        [HttpPost]
        public ActionResult Create(Card card)
        {
            try
            {
                var response = httpClient.PostAsJsonAsync("bee2be/servlet/api/card", card).
                    ContinueWith(task=>task.Result.EnsureSuccessStatusCode());
                
                return RedirectToAction("Index");
                
            }
            catch
            {
                return View();
            }
        }

        // GET: Card/Edit/5
        public ActionResult Edit(int id)
        {
            var response = httpClient.GetAsync("bee2be/servlet/api/card/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsAsync<Card>().Result;
                return View(content);
            }
            else
            {
                return View(new Card());
            }
        }

        // POST: Card/Edit/5
        [HttpPut]
        public ActionResult Edit(int id, Card card)
        {
            try
            {
                var response = httpClient.PutAsJsonAsync("bee2be/servlet/api/card/"+id, card).
                    ContinueWith(task => task.Result.EnsureSuccessStatusCode());

                return RedirectToAction("Index");

            }
            catch
            {
                return View();
            }
        }

        // GET: Card/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Card/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
