﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionBancaireFrontEnd.Models
{
    public class Account
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("accountKey")]
        public string AccountKey { get; set; }

        [JsonProperty("rib")]
        public string Rib { get; set; }

        [JsonProperty("iban")]
        public string Iban { get; set; }

        [JsonProperty("balance")]
        public double Balance { get; set; }

        [JsonProperty("creditScore")]
        public int CreditScore { get; set; }

        [JsonProperty("expirationDate")]
        public string ExpirationDate { get; set; }

        [JsonProperty("holder")]
        public string Holder { get; set; }

        [JsonProperty("clientType")]
        public string ClientType { get; set; }

        [JsonProperty("accountType")]
        public string AccountType { get; set; }

        [JsonProperty("insertedAt")]
        public object InsertedAt { get; set; }

        [JsonProperty("particularClient")]
        public object ParticularClient { get; set; }

        [JsonProperty("entrepriseClient")]
        public object EntrepriseClient { get; set; }

        [JsonProperty("agency")]
        public object Agency { get; set; }

        public static implicit operator Account(string v)
        {
            throw new NotImplementedException();
        }
    }
}