﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionBancaireFrontEnd.Models
{
    public class City
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("zipCode")]
        public string ZipCode { get; set; }

        [JsonProperty("insertedAt")]
        public string InsertedAt { get; set; }

        [JsonProperty("country")]
        public Country Country { get; set; }
    }
}