﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionBancaireFrontEnd.Models
{
    public class Country
    {
            [JsonProperty("id")]
            public int Id { get; set; }

            [JsonProperty("countryName")]
            public string CountryName { get; set; }

            [JsonProperty("countryCode")]
            public string CountryCode { get; set; }

            [JsonProperty("insertedAt")]
            public string InsertedAt { get; set; }


    }
}