﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionBancaireFrontEnd.Models
{
    public class Card
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("insertedAt")]
        public string InsertedAt { get; set; }

        [JsonProperty("account")]
        public Account Account { get; set; }

        [JsonProperty("cardNumber")]
        public string CardNumber { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("cardHolderFullName")]
        public string CardHolderFullName { get; set; }

        [JsonProperty("ccv")]
        public string Ccv { get; set; }

        [JsonProperty("pinNumber")]
        public string PinNumber { get; set; }

        [JsonProperty("expirationDate")]
        public string ExpirationDate { get; set; }

        [JsonProperty("cardStatus")]
        public string CardStatus { get; set; }

        [JsonProperty("limited")]
        public bool Limited { get; set; }

        [JsonProperty("spendingLimitPerMonth")]
        public double SpendingLimitPerMonth { get; set; }

        [JsonProperty("network")]
        public string Network { get; set; }

        [JsonProperty("cardType")]
        public CardType CardType { get; set; }


        /*        [JsonProperty("memberships")]
                public List<Membership> Memberships { get; set; }

                [JsonProperty("movements")]
                public List<Movement> Movements { get; set; }*/

    }
}