﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionBancaireFrontEnd.Models
{
    public class CardType
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("logo")]
        public object Logo { get; set; }

        [JsonProperty("background")]
        public string Background { get; set; }

        [JsonProperty("maximumWithdrawalPerWeek")]
        public double MaximumWithdrawalPerWeek { get; set; }

        [JsonProperty("feesPerYear")]
        public double FeesPerYear { get; set; }

        [JsonProperty("validationYear")]
        public int ValidationYear { get; set; }

        [JsonProperty("allowNegativeBalance")]
        public bool AllowNegativeBalance { get; set; }

        [JsonProperty("negativeBalanceLimit")]
        public double NegativeBalanceLimit { get; set; }

        [JsonProperty("negativeBalanceFees")]
        public double NegativeBalanceFees { get; set; }

        [JsonProperty("insertedAt")]
        public string InsertedAt { get; set; }
    }
}