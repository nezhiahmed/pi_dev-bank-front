﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionBancaireFrontEnd.Models
{
    public class Location
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("longitude")]
        public double Longitude { get; set; }

        [JsonProperty("latitude")]
        public double Latitude { get; set; }

        [JsonProperty("zoom")]
        public double Zoom { get; set; }

        [JsonProperty("insertedAt")]
        public string InsertedAt { get; set; }

        [JsonProperty("city")]
        public City City { get; set; }
    }
}